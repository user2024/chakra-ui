import { LOCAL_ITEM_KEY_PERMISSIONS } from '../common/Constants';

export function savePermission(permissions) {
  if (permissions) {
    localStorage.setItem(LOCAL_ITEM_KEY_PERMISSIONS, permissions);
  }
}

export function checkPermission(auth) {
  if (!auth) {
    return false;
  }
  const permissions = localStorage.getItem(LOCAL_ITEM_KEY_PERMISSIONS);
  if (permissions) {
    return permissions.split(' ').includes(auth);
  }
  return false;
}
