import fetch from 'dva/fetch';
import { notification } from 'antd';
import { routerRedux } from 'dva/router';
import store from '../index';
import { errorMsg, successMsg } from './utils';
import { RequestMethod } from '../common/Enum';

const codeMessage = {
  200: '服务器成功返回请求的数据。',
  201: '新建或修改数据成功。',
  202: '一个请求已经进入后台排队（异步任务）。',
  204: '删除数据成功。',
  400: '发出的请求有错误，服务器没有进行新建或修改数据的操作。',
  401: '用户没有权限（令牌、用户名、密码错误）。',
  403: '用户得到授权，但是访问是被禁止的。',
  404: '发出的请求针对的是不存在的记录，服务器没有进行操作。',
  406: '请求的格式不可得。',
  410: '请求的资源被永久删除，且不会再得到的。',
  422: '当创建一个对象时，发生一个验证错误。',
  500: '服务器发生错误，请检查服务器。',
  502: '网关错误。',
  503: '服务不可用，服务器暂时过载或维护。',
  504: '网关超时。',
};

function checkStatus(response) {
  if (!response) {
    return response;
  }
  if (response.status >= 200 && response.status < 300) {
    return response;
  }
  const errortext = codeMessage[response.status] || response.statusText;
  notification.error({
    message: `请求错误 ${response.status}: ${response.url}`,
    description: errortext,
  });
  const error = new Error(errortext);
  error.name = response.status;
  error.response = response;
  throw error;
}

const defaultOptions = {
  credentials: 'include',
  xhrFields: {
    withCredentials: true,
  },
  ...RequestMethod.get,
  tips: true, // 操作完成后有消息提示。
};

/**
 * 默认不提示消息的请求
 * @param url
 * @param options
 * @returns {Object}
 */
export function requestNoTips(url, options) {
  const newOptions = { tips: false, ...options };
  return request(url, newOptions);
}

/**
 * 默认Post方式的请求
 * @param url
 * @param options
 * @returns {Object}
 */
export function requestPost(url, options) {
  const newOptions = { ...RequestMethod.post, ...options };
  return request(url, newOptions);
}

/**
 * 默认get方式的请求
 * @param url
 * @param options
 * @returns {Object}
 */
export function requestGet(url, options) {
  const newOptions = { ...RequestMethod.get, ...options };
  return request(url, newOptions);
}

/**
 * 默认Put方式的请求
 * @param url
 * @param options
 * @returns {Object}
 */
export function requestPut(url, options) {
  const newOptions = { ...RequestMethod.put, ...options };
  return request(url, newOptions);
}

/**
 * 默认Put方式的请求
 * @param url
 * @param options
 * @returns {Object}
 */
export function requestDelete(url, options) {
  const newOptions = { ...RequestMethod.delete, ...options };
  return request(url, newOptions);
}

/**
 * Requests a URL, returning a promise.
 *
 * fetch官网：https://developer.mozilla.org/es/docs/Web/API/Fetch_API/Utilizando_Fetch
 *
 * @param  {string} url       The URL we want to request
 * @param  {object} [options] The options we want to pass to "fetch"
 * @return {object}           An object containing either "data" or "err"
 */
function request(url, options) {
  const newOptions = { ...defaultOptions, ...options };
  if (newOptions.method === 'POST' || newOptions.method === 'PUT') {
    if (!(newOptions.body instanceof FormData)) {
      newOptions.headers = {
        Accept: 'application/json',
        'Content-Type': 'application/json; charset=utf-8',
        ...newOptions.headers,
      };
      newOptions.body = JSON.stringify(newOptions.body);
    } else {
      // newOptions.body is FormData
      newOptions.headers = {
        Accept: 'application/json',
        ...newOptions.headers,
      };
    }
  }

  return fetch(url, newOptions)
    .then(checkStatus)
    .then(response => {
      if (response && (newOptions.method === 'DELETE' || response.status === 204)) {
        return response.text();
      }
      return response
        .json()
        .then(result => {
          if (!result) {
            return result;
          }
          const newResult = { ...result };
          newResult.success = result.code === 1;
          newResult.error = !newResult.success;
          return newResult;
        })
        .then(result => {
          if (newOptions.tips && result) {
            if (result.success) {
              successMsg();
            } else {
              errorMsg(result.message);
            }
          }
          return result;
        });
    })
    .catch(e => {
      const { dispatch } = store;
      const status = e.name;
      if (status === 401) {
        dispatch({
          type: 'login/logout',
        });
        return;
      }
      if (status === 403) {
        dispatch(routerRedux.push('/exception/403'));
        return;
      }
      if (status <= 504 && status >= 500) {
        dispatch(routerRedux.push('/exception/500'));
        return;
      }
      if (status >= 404 && status < 422) {
        dispatch(routerRedux.push('/exception/404'));
      }
    });
}
