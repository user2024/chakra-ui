import React from 'react';
import { Select } from 'antd';

const { Option } = Select;

export function getEnumElements(nameCodeEnum, disabledValues) {
  if (nameCodeEnum) {
    const rows = [];
    Object.keys(nameCodeEnum).forEach(key => {
      const obj = nameCodeEnum[key];
      rows.push({
        label: obj.name,
        value: obj.code,
        disabled: disabledValues != null && disabledValues.includes(obj.value),
      });
    });
    return rows;
  }
}

export function getEnumSelectOptions(nameCodeEnum, disabledValues) {
  if (nameCodeEnum) {
    const rows = [];
    Object.keys(nameCodeEnum).forEach(key => {
      const obj = nameCodeEnum[key];
      const disabled = disabledValues != null && disabledValues.includes(obj.code);
      rows.push(
        <Option key={obj.code} value={obj.code} disabled={disabled}>
          {obj.name}
        </Option>
      );
    });
    return rows;
  }
}
