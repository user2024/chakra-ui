// jwt存入localstorage的key
export const COOKIES_KEY_JWT = 'HESHUN_JWT';
export const LOCAL_ITEM_KEY_PERMISSIONS = 'HESHUN_PERMISSION';
export const SECOND_ONE_DAY = 86400;
export const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 7 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 12 },
    md: { span: 10 },
  },
};
