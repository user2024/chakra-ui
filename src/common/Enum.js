export const RequestMethod = {
  get: { method: 'GET' },
  post: { method: 'POST' },
  put: { method: 'PUT' },
  delete: { method: 'DELETE' },
};

export const UrlPrefix = {
  upms: {
    permission: {
      permissions: '/api/upms/manage/permission/permissions',
      roles: '/api/upms/manage/permission/roles',
    },
  },
};

export const defaultPagination = {
  pageNum: 1,
  pageSize: 10,
};

export function codeOf(obj, code) {
  return obj[Object.keys(obj).find(o => obj[o].code === code)];
}

export const PermissionType = {
  menu: { name: '菜单', code: 1 },
  resource: { name: '资源', code: 2 },
};

export const PermissionStatus = {
  normal: { name: '正常', code: 1 },
  disabled: { name: '已禁用', code: 2 },
};
export const PermissionLocation = {
  left: { name: '左边', code: 1 },
  top: { name: '顶部', code: 2 },
};

export const PermissionOpenWith = {
  center: { name: '内容区', code: 1 },
  newTab: { name: '新标签页', code: 2 },
};
