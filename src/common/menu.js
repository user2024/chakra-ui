import { isUrl } from '../utils/utils';
import { PermissionType } from './Enum';

export function formatMenu(data, parentPath = '/', parentAuthority) {
  return data.map(item => {
    let { path } = item;
    if (!isUrl(path)) {
      path = parentPath + item.path;
    }
    const result = {
      ...item,
      path,
      authority: item.permissionValue || parentAuthority,
    };
    if (item.children) {
      result.children = formatMenu(item.children, `${parentPath}${item.path}/`, item.authority);
    }
    return result;
  });
}

export const redirectData = [];

/**
 * 根据菜单取得重定向地址.
 */
export const getRedirectData = () => {
  return redirectData;
};

export const getRedirect = item => {
  if (item && item.children) {
    if (item.type !== 3) {
      if (item.children[0] && item.children[0].path) {
        redirectData.push({
          from: `${item.path}`,
          to: `${item.children[0].path}`,
        });
        item.children.forEach(children => {
          getRedirect(children);
        });
      }
    }
  }
};

export function generateTreeMenus(menus) {
  for (const menu of menus) {
    if (PermissionType.resource.code !== menu.type.code) {
      const children = menus.filter(n => parseFloat(n.pid) === parseFloat(menu.id));
      if (children && children.length) {
        menu.children = children;
      }
      generateTreeMenus(children);
    }
  }
  return menus.filter(n => n.pid === 0);
}
