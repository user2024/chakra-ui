import React, { Component } from 'react';
import { connect } from 'dva';
import { Form, Input, Radio, InputNumber, TreeSelect } from 'antd';
import {
  PermissionLocation,
  PermissionOpenWith,
  PermissionStatus,
  PermissionType,
} from '../../../../common/Enum';
import { formItemLayout } from '../../../../common/Constants';
import PermissionNameFormItem from './PermissionNameFormItem';
import { FORM_CN_OPTION } from '../../../../common/newMessages-cn';
import { getEnumElements } from '../../../../common/EnumUtil';
import { PermissionAction } from '../../../../models/upms/permission/permission';
import BasicCUModal from '../../BasicAdmin/BasicCUModal';

const FormItem = Form.Item;

@connect(state => ({ permission: state.permission }))
@Form.create(FORM_CN_OPTION)
export default class PermissionEditModal extends Component {
  static defaultProps = {
    model: {
      type: PermissionType.menu,
      location: PermissionLocation.left,
      openWith: PermissionOpenWith.center,
      status: PermissionStatus.normal,
    },
  };

  state = {
    permissions: [],
  };

  componentWillMount() {
    const { dispatch } = this.props;
    dispatch({
      ...PermissionAction.query,
      callback: tableData => {
        this.setState({
          permissions: tableData.dataSource,
        });
      },
    });
  }

  handleBeforeSubmit = values => {
    if (!values.pid) {
      // eslint-disable-next-line no-param-reassign
      values.pid = 0;
    }
  };

  menuTypeIsResource() {
    const menuTypeCode = this.props.form.getFieldValue('type');
    return menuTypeCode === PermissionType.resource.code;
  }

  render() {
    const { children, form, form: { getFieldDecorator }, model, dispatch } = this.props;
    const { permissions } = this.state;
    const title = model.id ? '编辑权限' : '创建权限';
    return (
      <BasicCUModal
        button={children}
        title={title}
        action={PermissionAction}
        model={model}
        form={form}
        dispatch={dispatch}
        handleQuery={this.props.handleQuery}
        beforeSubmit={this.handleBeforeSubmit}
      >
        {getFieldDecorator('status', {
          initialValue: model.status.code,
        })(<input type="hidden" name="status" />)}

        <FormItem {...formItemLayout} label="父级权限">
          {getFieldDecorator('pid', {
            initialValue: model.pid === 0 ? null : model.pid,
          })(
            <TreeSelect
              dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
              treeData={permissions}
              allowClear
              placeholder="请选择"
              treeDefaultExpandAll
            />
          )}
        </FormItem>
        <PermissionNameFormItem
          form={this.props.form}
          formItemLayout={formItemLayout}
          value={model.name}
        />
        <FormItem {...formItemLayout} label="路径">
          {getFieldDecorator('path', {
            initialValue: model.path,
            rules: [
              {
                max: 255,
              },
            ],
          })(<Input placeholder="权限字符串" />)}
        </FormItem>

        <FormItem {...formItemLayout} label="权限类型">
          {getFieldDecorator('type', {
            initialValue: model.type.code,
          })(<Radio.Group options={getEnumElements(PermissionType)} />)}
        </FormItem>

        <FormItem {...formItemLayout} label="打开位置">
          {getFieldDecorator('location', {
            initialValue: model.location.code,
          })(
            <Radio.Group
              options={getEnumElements(PermissionLocation)}
              disabled={this.menuTypeIsResource()}
            />
          )}
        </FormItem>

        <FormItem {...formItemLayout} label="打开方式">
          {getFieldDecorator('openWith', {
            initialValue: model.openWith.code,
          })(
            <Radio.Group
              options={getEnumElements(PermissionOpenWith)}
              disabled={this.menuTypeIsResource()}
            />
          )}
        </FormItem>

        <FormItem {...formItemLayout} label="图标">
          {getFieldDecorator('icon', {
            initialValue: model.icon,
          })(
            <Input.Search
              placeholder="点击按钮跳转参考网站"
              onSearch={() => window.open('https://ant.design/components/icon-cn/')}
              enterButton
            />
          )}
        </FormItem>
        <FormItem {...formItemLayout} label="排序">
          {getFieldDecorator('seq', {
            initialValue: model.seq,
          })(<InputNumber placeholder="请输入" min={0} max={999} />)}
        </FormItem>
      </BasicCUModal>
    );
  }
}
