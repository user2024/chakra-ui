import React, { PureComponent } from 'react';
import { Form } from 'antd';

const FormItem = Form.Item;

export default class RemoteValidateFormItem extends PureComponent {
  static defaultProps = {
    form: () => {},
    field: '',
    rules: [],
    label: '',
    action: {},
    formItemLayout: {},
    initialValue: null,
    payloadFields: [],
    errorMsg: '该项已存在',
  };

  remoteValidator = (rule, value, callback) => {
    if (value) {
      const { getFieldValue } = this.props.form;
      const { dispatch, payloadFields, field, errorMsg, action } = this.props;

      const payload = payloadFields.map(k => {
        return { k: getFieldValue(k) };
      });
      payload[field] = value;
      payload.id = getFieldValue('id');

      dispatch({
        ...action,
        payload,
        callback: response => {
          if (response) {
            callback(new Error(errorMsg));
          } else {
            callback();
          }
        },
      });
    } else {
      callback();
    }
  };

  render() {
    const {
      formItemLayout,
      form: { getFieldDecorator },
      initialValue,
      field,
      label,
      children,
    } = this.props;
    return (
      <FormItem {...formItemLayout} label={label}>
        {getFieldDecorator(field, {
          initialValue,
          rules: [
            {
              required: true,
            },
            {
              validator: this.remoteValidator,
            },
          ],
        })(children)}
      </FormItem>
    );
  }
}
