// 常用管理页面的布局
import React, { Component } from 'react';
import { Form, Input } from 'antd';
import { FORM_CN_OPTION } from '../../../../common/newMessages-cn';
import { PermissionAction } from '../../../../models/upms/permission/permission';
import BasicAdminQueryForm from '../../BasicAdmin/BasicAdminQueryForm';

const FormItem = Form.Item;

@Form.create(FORM_CN_OPTION)
export default class PermissionQueryForm extends Component {
  render() {
    const { form, form: { getFieldDecorator }, handleQuery } = this.props;
    return (
      <BasicAdminQueryForm handleQuery={handleQuery} action={PermissionAction} form={form}>
        <FormItem label="名称">{getFieldDecorator('name')(<Input placeholder="" />)}</FormItem>
        <FormItem label="路径">{getFieldDecorator('path')(<Input placeholder="" />)}</FormItem>
      </BasicAdminQueryForm>
    );
  }
}
