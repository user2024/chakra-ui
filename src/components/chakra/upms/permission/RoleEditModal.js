import React, { Component } from 'react';
import { connect } from 'dva';
import { Form, Input } from 'antd';
import { formItemLayout } from '../../../../common/Constants';
import { FORM_CN_OPTION } from '../../../../common/newMessages-cn';
import BasicCUModal from '../../BasicAdmin/BasicCUModal';
import { RoleAction } from '../../../../models/upms/permission/role';
import RemoteValidateFormItem from './RemoteValidateFormItem';

const FormItem = Form.Item;

@connect(state => ({ role: state.role }))
@Form.create(FORM_CN_OPTION)
export default class RoleEditModal extends Component {
  static defaultProps = {
    model: {},
  };

  render() {
    const { children, form, form: { getFieldDecorator }, model, dispatch } = this.props;
    const title = model.id ? '编辑角色' : '创建角色';
    return (
      <BasicCUModal
        button={children}
        title={title}
        action={RoleAction}
        model={model}
        form={form}
        dispatch={dispatch}
        handleQuery={this.props.handleQuery}
      >
        <RemoteValidateFormItem
          form={form}
          formItemLayout={{ ...formItemLayout }}
          dispatch={dispatch}
          field="name"
          label="名称"
          action={RoleAction.isNameExists}
          initialValue={model.name}
        >
          <Input placeholder="名称" />
        </RemoteValidateFormItem>
        <FormItem {...formItemLayout} label="描述">
          {getFieldDecorator('desc', {
            initialValue: model.desc,
            rules: [
              {
                max: 255,
              },
            ],
          })(<Input placeholder="描述" />)}
        </FormItem>
      </BasicCUModal>
    );
  }
}
