import React, { Component } from 'react';
import { connect } from 'dva';
import { Affix, Button, Table } from 'antd';
import { PermissionAction } from '../../../../models/upms/permission/permission';
import { renderStatus } from '../../../../routes/upms/permission/Permission';
import ChakraTable from '../../common/ChakraTable';
import BasicAdminButtons from '../../BasicAdmin/BasicAdminButtons';

const { Column } = Table;

@connect(state => ({ permission: state.permission }))
export default class PermissionTable extends Component {
  static defaultProps = {
    onAuthorized: () => {},
    selectedRowKeys: [],
  };

  state = {
    permissions: [],
    selectedRowKeys: [],
  };

  componentWillMount() {
    const { dispatch } = this.props;
    dispatch({
      ...PermissionAction.query,
      callback: tableData => {
        this.setState({
          permissions: tableData.dataSource,
        });
      },
    });
  }

  componentWillReceiveProps(nextProps) {
    const { selectedRowKeys } = nextProps;
    this.setState({ selectedRowKeys });
  }

  handelRowSelected = selectedRowKeys => {
    this.setState({ selectedRowKeys });
  };

  render() {
    const { permissions, selectedRowKeys } = this.state;
    return (
      <div>
        <BasicAdminButtons>
          <Affix>
            <Button
              type="primary"
              onClick={() => {
                this.props.onAuthorized(selectedRowKeys);
              }}
            >
              授权
            </Button>
          </Affix>
        </BasicAdminButtons>
        <ChakraTable
          title={() => '权限列表'}
          dataSource={permissions}
          pagination={false}
          rowSelection={{
            selectedRowKeys,
          }}
          // eslint-disable-next-line react/jsx-boolean-value
          selectOnClickRow={true}
          onRowSelected={this.handelRowSelected}
          {...this.props}
        >
          <Column title="资源名称" dataIndex="name" />
          <Column title="路径" dataIndex="path" />
          <Column title="类型" dataIndex="type" render={text => <span>{text.name}</span>} />
          <Column title="状态" dataIndex="status" render={renderStatus} />
        </ChakraTable>
      </div>
    );
  }
}
