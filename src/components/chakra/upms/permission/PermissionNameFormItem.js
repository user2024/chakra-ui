import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { Input, Form } from 'antd';
import PropTypes from 'prop-types';
import { PermissionAction } from '../../../../models/upms/permission/permission';

const FormItem = Form.Item;

@connect(state => ({ permission: state.permission }))
export default class PermissionNameFormItem extends PureComponent {
  static propTypes = {
    form: PropTypes.object.isRequired,
  };

  menuNameValidator = (rule, value, callback) => {
    if (value) {
      const { getFieldValue } = this.props.form;
      const { dispatch } = this.props;
      dispatch({
        ...PermissionAction.isNameExists,
        payload: { name: value, pid: getFieldValue('pid'), id: getFieldValue('id') },
        callback: response => {
          if (response) {
            callback(new Error('该名称已存在'));
          } else {
            callback();
          }
        },
      });
    } else {
      callback();
    }
  };

  render() {
    const { formItemLayout, form: { getFieldDecorator }, value } = this.props;
    return (
      <FormItem {...formItemLayout} label="名称">
        {getFieldDecorator('name', {
          initialValue: value,
          rules: [
            {
              required: true,
            },
            {
              validator: this.menuNameValidator,
            },
          ],
        })(<Input placeholder="给权限起个名字" />)}
      </FormItem>
    );
  }
}
