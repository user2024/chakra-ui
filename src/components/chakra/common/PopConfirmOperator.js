import React, { PureComponent } from 'react';
import { Popconfirm } from 'antd';

export default class PopConfirmOperator extends PureComponent {
  render() {
    return (
      <Popconfirm title="请确认是否执行该操作?" onConfirm={this.props.onConfirm}>
        <a>{this.props.text}</a>
      </Popconfirm>
    );
  }
}
