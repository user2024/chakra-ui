/* eslint-disable no-param-reassign */
import React, { Component } from 'react';
import { Table } from 'antd';
import styles from './ChakraTable.less';

function getRowKeys(dataSource) {
  let array = [];
  dataSource.forEach(d => {
    const { children } = d;
    if (children) {
      array = [...getRowKeys(children)];
    }
    array.push(d.key);
  });
  return array;
}

/**
 * 对表格进行扩展
 * 1.增加selectOnClickRow属性。如果该项为true，则用法给官方有一点不一致：
 * 官方用法：
 * onRow={(record) => {
    return {
      onClick: () => {},       // 点击行
      onMouseEnter: () => {},  // 鼠标移入行
      onXxxx...
    };
   }}
 * selectOnClickRow =true时候的用法：
 *
 * onRow={() => {
    return {
      onClick: (record) => {},       // 点击行
      onMouseEnter: (record) => {},  // 鼠标移入行
      onXxxx...
    };
   }}
 * 注意record放在return的object的函数中。这是由于onRow接收的是方法，无法直接扩展导致的，
 */
export default class ChakraTable extends Component {
  static defaultProps = {
    selectOnClickRow: false,
    onRowSelected: () => {},
  };

  state = {
    selectedRowKeys: [],
    rowClassName: () => {},
  };

  onSelectedRowKeysChange = (selectedRowKeys, selectedRows) => {
    this.setState({
      selectedRowKeys,
    });
    const { onRowSelected } = this.props;
    if (onRowSelected) {
      onRowSelected(selectedRowKeys, selectedRows);
    }
  };

  getObj = (oldOnRow, record) => {
    const obj = {};
    Object.keys(oldOnRow).forEach(k => {
      if (k !== 'onClick') {
        obj[k] = () => {
          oldOnRow[k](record);
        };
      }
    });
    return obj;
  };

  setRowBg() {
    this.setState({
      rowClassName: () => {
        return styles.row;
      },
    });
  }

  /**
   * 扩展props，添加selectOnRow属性
   * @param newProps
   */
  extraSelectOnClickRow(newProps) {
    const { selectOnClickRow, onRow } = newProps;
    if (selectOnClickRow) {
      if (!onRow) {
        newProps.onRow = record => {
          return {
            onClick: () => {
              this.setRowBg();
              this.selectRow(record);
            },
          };
        };
      } else {
        const oldOnRow = onRow();
        const oldOnRowClick = oldOnRow.onClick;
        newProps.onRow = record => {
          return {
            onClick: () => {
              if (oldOnRowClick) oldOnRowClick(record);
              this.setRowBg();
              this.selectRow(record);
            },
            ...this.getObj(oldOnRow, record),
          };
        };
      }
      this.extraRowSelection(newProps);
    }
  }

  selectRow = record => {
    const selectedRowKeys = [...this.state.selectedRowKeys];
    if (selectedRowKeys.indexOf(record.key) >= 0) {
      selectedRowKeys.splice(selectedRowKeys.indexOf(record.key), 1);
    } else {
      selectedRowKeys.push(record.key);
    }
    this.setState({ selectedRowKeys });
  };

  extraRowSelection(newProps) {
    const { rowSelection } = newProps;
    let { selectedRowKeys } = this.state;
    if (rowSelection) {
      const { onChange, selectedRowKeys: defaultRowKeys } = rowSelection;
      if (defaultRowKeys) {
        selectedRowKeys = defaultRowKeys.map(k => `${k}`);
      }
      const newRowSelection = {
        ...rowSelection,
        selectedRowKeys,
        onChange: (rowKeys, selectedRows) => {
          if (onChange) onChange(rowKeys, selectedRows);
          this.onSelectedRowKeysChange(rowKeys, selectedRows);
        },
      };
      newProps.rowSelection = newRowSelection;
    } else {
      newProps.rowSelection = {
        selectedRowKeys,
        onChange: this.onSelectedRowKeysChange,
      };
    }
  }

  render() {
    const newProps = { ...this.props };
    const { defaultExpandAllRows, dataSource, expandedRowKeys } = newProps;

    // 解决默认的defaultExpandAllRows不生效的问题。
    if (defaultExpandAllRows) {
      if (!expandedRowKeys) {
        newProps.expandedRowKeys = getRowKeys(dataSource);
      }
    }

    this.extraSelectOnClickRow(newProps);
    return (
      <Table
        {...newProps}
        rowClassName={() => {
          return styles.row;
        }}
      />
    );
  }
}
