import React, { PureComponent } from 'react';
import { Modal, Form } from 'antd';
import { PropTypes } from 'prop-types';

export default class BasicAdminModal extends PureComponent {
  static propTypes = {
    title: PropTypes.string.isRequired,
    button: PropTypes.any.isRequired,
    onOk: PropTypes.func.isRequired,
    form: PropTypes.object.isRequired,
    children: PropTypes.any.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      confirmLoading: false,
    };
    this.handelShow = this.handelShow.bind(this);
  }

  handelShow = e => {
    if (e) e.stopPropagation();
    this.setState({
      visible: true,
    });
  };

  handleCancel = () => {
    this.props.form.resetFields();
    this.setState({
      visible: false,
    });
  };

  handleOk = () => {
    const { onOk, form: { validateFields } } = this.props;
    this.setState({
      confirmLoading: true,
    });
    validateFields((err, values) => {
      if (!err) {
        onOk(values);
        this.handleCancel();
      }
    });
    this.setState({
      confirmLoading: false,
    });
  };

  render() {
    const { button, title, children } = this.props;
    return (
      <span>
        <span onClick={this.handelShow}>{button}</span>
        <Modal
          title={title}
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
          maskClosable={false}
          confirmLoading={this.state.confirmLoading}
        >
          <Form>{children}</Form>
        </Modal>
      </span>
    );
  }
}
