import React, { PureComponent } from 'react';
import { Row, Col, Form, Button } from 'antd';
import { instanceOf, PropTypes } from 'prop-types';

export default class BasicAdminQueryForm extends PureComponent {
  static props = {
    form: instanceOf(Form).isRequired,
    children: PropTypes.element.isRequired,
    action: PropTypes.object,
  };

  handleSubmit = () => {
    const { form, handleQuery } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      if (handleQuery) {
        handleQuery(fieldsValue);
      }
    });
  };

  handleFormReset = () => {
    const { form, handleQuery } = this.props;
    form.resetFields();
    if (handleQuery) handleQuery({});
  };

  genCol = () => {
    const { children } = this.props;
    return children.map((item, i) => {
      const key = i;
      return (
        <Col md={8} sm={24} key={key}>
          {item}
        </Col>
      );
    });
  };

  render() {
    return (
      <Form onSubmit={this.handleSubmit.bind(this)} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          {this.genCol()}
          <Col md={8} sm={24}>
            <span className="submitButtons">
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset.bind(this)}>
                重置
              </Button>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }
}
