import React from 'react';
import { Divider } from 'antd';

export default () => {
  return <Divider type="vertical" />;
};
