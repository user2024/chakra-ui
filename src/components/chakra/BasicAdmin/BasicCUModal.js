import React, { Component } from 'react';
import {
  PermissionLocation,
  PermissionOpenWith,
  PermissionStatus,
  PermissionType,
} from '../../../common/Enum';
import BasicAdminModal from './BasicAdminModal';

export default class BasicCUModal extends Component {
  static defaultProps = {
    title: '',
    handleQuery: () => {},
    beforeSubmit: () => {},
    model: {
      type: PermissionType.menu,
      location: PermissionLocation.left,
      openWith: PermissionOpenWith.center,
      status: PermissionStatus.normal,
    },
    action: {},
    form: {},
    dispatch: () => {},
  };

  handleCreate = values => {
    const { dispatch, handleQuery, action } = this.props;
    dispatch({ ...action.create, payload: values });
    handleQuery({});
  };

  handleEdit = values => {
    const { dispatch, handleQuery, action } = this.props;
    dispatch({ ...action.edit, payload: values });
    handleQuery();
  };

  handleSubmit = values => {
    const beforeSubmit = this.props.beforeSubmit(values);
    if (beforeSubmit === false) {
      return;
    }
    if (values.id) {
      this.handleEdit(values);
    } else {
      this.handleCreate(values);
    }
  };

  render() {
    const { children, form: { getFieldDecorator }, title, model, button, form } = this.props;
    return (
      <BasicAdminModal
        button={button}
        title={title}
        onOk={this.handleSubmit.bind(this)}
        form={form}
      >
        {getFieldDecorator('id', {
          initialValue: model.id,
        })(<input type="hidden" name="id" />)}

        {children}
      </BasicAdminModal>
    );
  }
}
