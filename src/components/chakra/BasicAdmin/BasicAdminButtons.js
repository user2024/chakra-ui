import React from 'react';
import styles from '../../../layouts/BasicAdminLayout.less';

export default ({ children }) => {
  return (
    <div className={styles.tableListOperator}>
      <span>{children}</span>
    </div>
  );
};
