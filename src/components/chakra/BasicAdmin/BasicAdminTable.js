import React, { PureComponent, Fragment } from 'react';
import { Alert } from 'antd';
import styles from './BasicAdminTable.less';
import ChakraTable from '../common/ChakraTable';

function initTotalList(columns) {
  const totalList = [];
  columns.forEach(column => {
    if (column.needTotal) {
      totalList.push({ ...column, total: 0 });
    }
  });
  return totalList;
}

export default class BasicAdminTable extends PureComponent {
  constructor(props) {
    super(props);
    const { tableProps: { columns } } = props;
    const needTotalList = initTotalList(columns);
    this.state = {
      selectedRowKeys: [],
      needTotalList,
    };
  }

  componentWillReceiveProps(nextProps) {
    // clean state
    if (nextProps.selectedRows && nextProps.selectedRows.length === 0) {
      const needTotalList = initTotalList(nextProps.tableProps.columns);
      this.setState({
        selectedRowKeys: [],
        needTotalList,
      });
    }
  }

  getAlert() {
    const { selectedRowKeys, needTotalList } = this.state;
    const { tableProps: { rowSelection } } = this.props;
    if (rowSelection) {
      return (
        <div className={styles.tableAlert}>
          <Alert
            message={
              <Fragment>
                已选择 <a style={{ fontWeight: 600 }}>{selectedRowKeys.length}</a> 项&nbsp;&nbsp;
                {needTotalList.map(item => (
                  <span style={{ marginLeft: 8 }} key={item.dataIndex}>
                    {item.title}总计&nbsp;
                    <span style={{ fontWeight: 600 }}>
                      {item.render ? item.render(item.total) : item.total}
                    </span>
                  </span>
                ))}
                <a onClick={this.cleanSelectedKeys} style={{ marginLeft: 24 }}>
                  清空
                </a>
              </Fragment>
            }
            type="info"
            showIcon
          />
        </div>
      );
    } else {
      return null;
    }
  }

  getTableProps = () => {
    const { selectedRowKeys } = this.state;
    const { tableProps } = this.props;
    const { pagination, rowSelection } = tableProps;

    let paginationProps = pagination;
    if (pagination !== false) {
      paginationProps = {
        showSizeChanger: true,
        showQuickJumper: true,
        ...pagination,
      };
    }
    tableProps.rowKey = tableProps.rowKey || 'id';
    tableProps.pagination = paginationProps;
    if (rowSelection) {
      tableProps.rowSelection = {
        selectedRowKeys,
        onChange: this.handleRowSelectChange,
        getCheckboxProps: record => ({
          disabled: record.disabled,
        }),
      };
    }
    return tableProps;
  };

  cleanSelectedKeys = () => {
    this.handleRowSelectChange([], []);
  };

  handleRowSelectChange = (selectedRowKeys, selectedRows) => {
    let needTotalList = [...this.state.needTotalList];
    needTotalList = needTotalList.map(item => {
      return {
        ...item,
        total: selectedRows.reduce((sum, val) => {
          return sum + parseFloat(val[item.dataIndex], 10);
        }, 0),
      };
    });

    if (this.props.onSelectRow) {
      this.props.onSelectRow(selectedRows);
    }
    this.setState({ selectedRowKeys, needTotalList });
  };

  render() {
    return (
      <div className={styles.standardTable}>
        {this.getAlert()}
        <ChakraTable {...this.getTableProps()} />
      </div>
    );
  }
}
