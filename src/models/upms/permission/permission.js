import { genTreeData } from '../../../utils/utils';
import {
  create,
  del,
  disable,
  edit,
  enable,
  nameExists,
  query,
} from '../../../services/upms/permission/permisison';
import { generateTreeMenus } from '../../../common/menu';

export const PermissionAction = {
  query: { type: 'permission/query' },
  disable: { type: 'permission/disable' },
  create: { type: 'permission/create' },
  edit: { type: 'permission/edit' },
  enable: { type: 'permission/enable' },
  delete: { type: 'permission/delete' },
  isNameExists: { type: 'permission/isNameExists' },
};

function genTableData(list) {
  const treeObj = generateTreeMenus(list);
  const data = { dataSource: genTreeData(treeObj) };
  return data;
}

export default {
  namespace: 'permission',
  state: {
    list: [],
    tableData: {
      dataSource: [],
      pagination: false,
    },
    loading: true,
  },

  effects: {
    *query({ payload, callback }, { call, put }) {
      yield put({
        type: 'changeLoading',
        payload: true,
      });
      const response = yield call(query, payload);
      yield put({
        type: 'save',
        payload: response.data,
      });
      yield put({
        type: 'changeLoading',
        payload: false,
      });
      if (callback) {
        callback(genTableData(response.data));
      }
    },
    *isNameExists({ payload, callback }, { call }) {
      const response = yield call(nameExists, payload);
      if (callback) {
        callback(response.data);
      }
    },
    *create({ payload, callback }, { call }) {
      const response = yield call(create, payload);
      if (callback) {
        callback(response);
      }
    },
    *edit({ payload, callback }, { call }) {
      const response = yield call(edit, payload);
      if (callback) {
        callback(response);
      }
    },
    *delete({ payload, callback }, { call }) {
      const response = yield call(del, payload);
      if (callback) {
        callback(response);
      }
    },
    *disable({ payload }, { call }) {
      yield call(disable, payload);
    },
    *enable({ payload }, { call }) {
      yield call(enable, payload);
    },
  },

  reducers: {
    save(state, action) {
      const { tableData } = state;
      tableData.dataSource = genTableData(action.payload);
      return {
        ...state,
        tableData,
      };
    },
    changeLoading(state, action) {
      return {
        ...state,
        loading: action.payload,
      };
    },
  },
};
