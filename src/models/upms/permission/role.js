import { genPaginationTableData } from '../../../utils/utils';
import {
  create,
  del,
  edit,
  listPermissionIds,
  nameExists,
  query,
} from '../../../services/upms/permission/role';

export const RoleAction = {
  query: { type: 'role/query' },
  page: { type: 'role/page' },
  disable: { type: 'role/disable' },
  create: { type: 'role/create' },
  edit: { type: 'role/edit' },
  delete: { type: 'role/delete' },
  isNameExists: { type: 'role/isNameExists' },
  listPermissionIds: { type: 'role/listPermissionIds' },
};

export default {
  namespace: 'role',
  state: {
    tableData: {
      dataSource: [],
      pagination: {},
    },
    loading: true,
  },

  effects: {
    *query({ payload, callback }, { call, put }) {
      yield put({
        type: 'changeLoading',
        payload: true,
      });
      const response = yield call(query, payload);
      yield put({
        type: 'save',
        payload: response.data,
      });
      yield put({
        type: 'changeLoading',
        payload: false,
      });
      if (callback) {
        callback(genPaginationTableData(response.data));
      }
    },
    *isNameExists({ payload, callback }, { call }) {
      const response = yield call(nameExists, payload);
      if (callback) callback(response.data);
    },
    *create({ payload, callback }, { call }) {
      const response = yield call(create, payload);
      if (callback) {
        callback(response);
      }
    },
    *edit({ payload, callback }, { call }) {
      const response = yield call(edit, payload);
      if (callback) callback(response);
    },
    *delete({ payload, callback }, { call }) {
      const response = yield call(del, payload);
      if (callback) callback(response);
    },
    *listPermissionIds({ payload, callback }, { call }) {
      const response = yield call(listPermissionIds, payload);
      if (callback) callback(response.data);
    },
  },

  reducers: {
    save(state, action) {
      const data = genPaginationTableData(action.payload);
      return {
        ...state,
        tableData: data,
      };
    },
    changeLoading(state, action) {
      return {
        ...state,
        loading: action.payload,
      };
    },
  },
};
