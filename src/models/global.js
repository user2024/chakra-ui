import { fetchIndex, queryNotices } from '../services/api';
import { formatMenu, generateTreeMenus, getRedirect } from '../common/menu';

export default {
  namespace: 'global',

  state: {
    collapsed: false,
    notices: [],
    menus: [],
    currentUser: {},
    appName: null,
  },

  effects: {
    *fetchNotices(_, { call, put }) {
      const data = yield call(queryNotices);
      yield put({
        type: 'saveNotices',
        payload: data,
      });
      yield put({
        type: 'user/changeNotifyCount',
        payload: data.length,
      });
    },
    *clearNotices({ payload }, { put, select }) {
      yield put({
        type: 'saveClearedNotices',
        payload,
      });
      const count = yield select(state => state.global.notices.length);
      yield put({
        type: 'user/changeNotifyCount',
        payload: count,
      });
    },
    *fetchIndex(_, { call, put }) {
      const response = yield call(fetchIndex);
      yield put({
        type: 'saveIndex',
        payload: response.data,
      });
    },
  },

  reducers: {
    changeLayoutCollapsed(state, { payload }) {
      return {
        ...state,
        collapsed: payload,
      };
    },
    saveNotices(state, { payload }) {
      return {
        ...state,
        notices: payload,
      };
    },
    saveClearedNotices(state, { payload }) {
      return {
        ...state,
        notices: state.notices.filter(item => item.type !== payload),
      };
    },
    saveIndex(state, { payload }) {
      let permissions = payload.upmsPermissions;
      if (permissions) {
        permissions = generateTreeMenus(permissions);
        permissions = formatMenu(permissions);
        permissions.forEach(getRedirect);
      }
      return {
        ...state,
        menus: permissions,
        currentUser: payload.currentUser,
        appName: payload.appName,
      };
    },
  },

  subscriptions: {
    setup({ history }) {
      // Subscribe history(url) change, trigger `load` action if pathname is `/`
      return history.listen(({ pathname, search }) => {
        if (typeof window.ga !== 'undefined') {
          window.ga('send', 'pageview', pathname + search);
        }
      });
    },
  },
};
