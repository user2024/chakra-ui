import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { Button } from 'antd';
import injectAuth from '../InjectAuth/index';

export default class AuthButton extends Component {
  static propTypes = {
    auth: PropTypes.string.isRequired,
  };

  render() {
    const AuthedComponent = injectAuth(Button);
    if (AuthedComponent) {
      return <AuthedComponent {...this.props} />;
    }
    return null;
  }
}
