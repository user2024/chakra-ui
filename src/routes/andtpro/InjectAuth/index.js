import React from 'react';
import { checkPermission } from '../../../utils/AuthUtil';

export const noAuthType = {
  hidden: 'hidden',
  disabled: 'disabled',
};

export default function injectAuth(component) {
  const checkAuth = props => {
    const { auth } = props;
    // No Auth required
    if (auth == null) {
      return false;
    }
    return checkPermission(auth);
  };

  const WrappedClass = class extends component {
    render() {
      if (checkAuth(this.props)) {
        // todo 这里应该将auth和noauthtype属性从dom中去掉。
        return super.render();
      } else {
        const { noauthtype } = this.props || 'hidden';
        if (noauthtype === 'hidden') {
          return null;
        } else if (noauthtype === 'disabled') {
          // React doesn't allow to modify props like this.prpos.disabled=ture
          // Should clone the element to with inital props
          // See: http://stackoverflow.com/questions/32370994/how-to-pass-props-to-this-props-children
          return React.cloneElement(super.render(), {
            disabled: true,
            noauthtype: null,
            auth: null,
          });
        } else {
          throw new Error('noAuthType必须是hidden或disabled');
        }
      }
    }
  };

  return WrappedClass;
}
