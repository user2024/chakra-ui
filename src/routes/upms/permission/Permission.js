import React, { Fragment, Component } from 'react';
import { connect } from 'dva';
import { Form, Button, Icon, Badge } from 'antd';
import BasicAdminLayout from '../../../layouts/BasicAdminLayout';
import PermissionEditModal from '../../../components/chakra/upms/permission/PermissionEditModal';
import BasicAdminTable from '../../../components/chakra/BasicAdmin/BasicAdminTable';
import { FORM_CN_OPTION } from '../../../common/newMessages-cn';
import { PermissionStatus } from '../../../common/Enum';
import PopConfirmOperator from '../../../components/chakra/common/PopConfirmOperator';
import { PermissionAction } from '../../../models/upms/permission/permission';
import { isNotEmptyObj } from '../../../utils/utils';
import PermissionQueryForm from '../../../components/chakra/upms/permission/PermissionQueryForm';
import BasicAdminTableDivider from '../../../components/chakra/BasicAdmin/BasicAdminTableDivider';
import BasicAdminButtons from '../../../components/chakra/BasicAdmin/BasicAdminButtons';

function filter(params, list) {
  const result = list.filter(obj => {
    const newOne = { ...obj };
    const isNameMatch = newOne.name.includes(params.name) || !params.name;
    const isStatusMatch = newOne.status.code === params.status || !params.status;
    if (newOne.children) {
      newOne.children = filter(params, newOne.children);
    }
    if ((isNameMatch && isStatusMatch) || (newOne.children && newOne.children.length)) {
      return true;
    }
    return false;
  });
  return result;
}

export function renderStatus(status) {
  let badgeStatus;
  if (PermissionStatus.normal.code === status.code) {
    badgeStatus = 'success';
  } else {
    badgeStatus = 'default';
  }
  return <Badge status={badgeStatus} text={status.name} />;
}

const action = PermissionAction;

@connect(state => ({ permission: state.permission }))
@Form.create(FORM_CN_OPTION)
export default class Permission extends Component {
  constructor(props) {
    super(props);
    this.handleQuery = this.handleQuery.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
  }

  state = {
    selectedRows: [],
    tableData: {
      dataSource: [],
    },
    queryParams: {},
    fullTableList: [],
    expandAll: true,
  };

  componentDidMount() {
    this.handleQuery();
  }

  getStatusOperator = model => {
    switch (model.status.code) {
      case PermissionStatus.normal.code:
        return (
          <PopConfirmOperator
            text="禁用"
            onConfirm={() => {
              this.handleDisable(model.id);
            }}
          />
        );
      default:
        return (
          <PopConfirmOperator
            text="启用"
            onConfirm={() => {
              this.handleEnable(model.id);
            }}
          />
        );
    }
  };

  getLastParamsIfNoParams(params) {
    // 如果没有参数，就用上一次的参数
    let { queryParams } = this.state;
    if (params) {
      queryParams = params;
    }
    return queryParams;
  }

  handleQuery(params) {
    const queryParams = this.getLastParamsIfNoParams(params);
    const { dispatch } = this.props;
    dispatch({
      ...action.query,
      payload: queryParams,
      callback: tableData => {
        this.setState({
          fullTableList: tableData.dataSource,
          queryParams,
        });

        if (isNotEmptyObj(queryParams)) {
          this.expand(queryParams);
        } else {
          this.setState({
            tableData,
          });
        }
      },
    });
  }

  refreshTable() {
    this.handleQuery();
  }

  expand(params) {
    const { tableData, fullTableList } = this.state;
    tableData.dataSource = filter(params, fullTableList);
    this.setState({
      tableData,
      expandAll: true,
    });
  }

  handleDelete = id => {
    const { dispatch } = this.props;
    dispatch({
      ...action.delete,
      payload: id,
    });
    this.refreshTable();
  };
  handleDisable = id => {
    const { dispatch } = this.props;
    dispatch({
      ...action.disable,
      payload: id,
    });
    this.refreshTable();
  };
  handleEnable = id => {
    const { dispatch } = this.props;
    dispatch({
      ...action.enable,
      payload: id,
    });
    this.refreshTable();
  };
  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  render() {
    const { permission: { loading: permissionLoading }, form } = this.props;
    const { selectedRows, tableData, expandAll } = this.state;
    const columns = [
      {
        title: '名称',
        dataIndex: 'name',
        render: (text, model) => (
          <span>
            <Icon type={model.icon} />
            {text}
          </span>
        ),
      },
      {
        title: '类型',
        dataIndex: 'type',
        render: text => <span>{text.name}</span>,
      },
      {
        title: '状态',
        dataIndex: 'status',
        render: renderStatus,
      },
      {
        title: '路径',
        dataIndex: 'path',
      },
      {
        title: '描述',
        dataIndex: 'description',
      },
      {
        title: '操作',
        render: (text, model) => (
          <Fragment>
            <PermissionEditModal model={model} handleQuery={this.handleQuery}>
              <a>编辑</a>
            </PermissionEditModal>
            <BasicAdminTableDivider />
            <PopConfirmOperator
              text="删除"
              onConfirm={() => {
                this.handleDelete(model.id);
              }}
            />
            <BasicAdminTableDivider />
            {this.getStatusOperator(model)}
          </Fragment>
        ),
      },
    ];
    const tableProps = {
      selectedRows,
      loading: permissionLoading,
      ...tableData,
      form,
      columns,
      pagination: false,
      onSelectRow: this.handleSelectRows,
      defaultExpandAllRows: expandAll,
    };
    return (
      <BasicAdminLayout
        title="权限管理"
        queryForm={<PermissionQueryForm handleQuery={this.handleQuery} />}
      >
        <BasicAdminButtons>
          <PermissionEditModal handleQuery={this.handleQuery}>
            <Button type="primary">创建权限</Button>
          </PermissionEditModal>
        </BasicAdminButtons>

        <BasicAdminTable
          tableProps={tableProps}
          selectedRows={selectedRows}
          onSelectRow={this.handleSelectRows}
        />
      </BasicAdminLayout>
    );
  }
}
