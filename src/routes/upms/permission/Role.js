import React, { Fragment, Component } from 'react';
import { connect } from 'dva';
import { Form, Button, Row, Col } from 'antd';
import BasicAdminLayout from '../../../layouts/BasicAdminLayout';
import BasicAdminTable from '../../../components/chakra/BasicAdmin/BasicAdminTable';
import { FORM_CN_OPTION } from '../../../common/newMessages-cn';
import PopConfirmOperator from '../../../components/chakra/common/PopConfirmOperator';
import PermissionQueryForm from '../../../components/chakra/upms/permission/PermissionQueryForm';
import BasicAdminTableDivider from '../../../components/chakra/BasicAdmin/BasicAdminTableDivider';
import { RoleAction } from '../../../models/upms/permission/role';
import { defaultPagination } from '../../../common/Enum';
import RoleEditModal from '../../../components/chakra/upms/permission/RoleEditModal';
import BasicAdminButtons from '../../../components/chakra/BasicAdmin/BasicAdminButtons';
import PermissionTable from '../../../components/chakra/upms/permission/PermissionTable';

const action = RoleAction;
// 从store中获取menu对象
@connect(state => ({ role: state.role }))
@Form.create(FORM_CN_OPTION)
export default class Role extends Component {
  constructor(props) {
    super(props);
    this.handleQuery = this.handleQuery.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
  }

  state = {
    tableData: {
      dataSource: [],
      pagination: false,
    },
    queryParams: {
      ...defaultPagination,
    },
    permissionIds: [],
  };

  componentDidMount() {
    this.handleQuery();
  }

  getLastParamsIfNoParams(params) {
    // 如果没有参数，就用上一次的参数
    let { queryParams } = this.state;
    if (params) {
      queryParams = params;
    }
    return queryParams;
  }

  handleQuery(params) {
    const queryParams = this.getLastParamsIfNoParams(params);
    const { dispatch } = this.props;
    dispatch({
      ...action.query,
      payload: queryParams,
      callback: tableData => {
        this.setState({
          tableData,
          queryParams,
        });
      },
    });
  }

  handleDelete = id => {
    const { dispatch } = this.props;
    dispatch({
      ...action.delete,
      payload: id,
    });
    this.handleQuery();
  };

  handelAuthorized = selectedPermissionIds => {
    console.log(selectedPermissionIds);
  };

  listPermissionIds = id => {
    const { dispatch } = this.props;
    dispatch({
      ...action.listPermissionIds,
      payload: id,
      callback: permissionIds => {
        this.setState({ permissionIds });
      },
    });
  };

  render() {
    const { role: { loading }, form } = this.props;
    const { tableData, permissionIds } = this.state;
    const columns = [
      {
        title: '名称',
        dataIndex: 'name',
      },
      {
        title: '描述',
        dataIndex: 'desc',
      },
      {
        title: '操作',
        render: (text, model) => (
          <Fragment>
            <RoleEditModal model={model} handleQuery={this.handleQuery}>
              <a>编辑</a>
            </RoleEditModal>
            <BasicAdminTableDivider />
            <PopConfirmOperator
              text="删除"
              onConfirm={() => {
                this.handleDelete(model.id);
              }}
            />
            <BasicAdminTableDivider />
          </Fragment>
        ),
      },
    ];
    const tableProps = {
      loading,
      ...tableData,
      form,
      columns,
      title: () => '角色列表',
      onRow: record => {
        return {
          onClick: () => {
            this.listPermissionIds(record.id);
          },
        };
      },
    };
    return (
      <BasicAdminLayout
        title="角色管理"
        queryForm={<PermissionQueryForm handleQuery={this.handleQuery} />}
      >
        <div>
          <Row gutter={16}>
            <Col span={10}>
              <BasicAdminButtons>
                <RoleEditModal handleQuery={this.handleQuery}>
                  <Button type="primary">创建角色</Button>
                </RoleEditModal>
              </BasicAdminButtons>
              <BasicAdminTable tableProps={tableProps} />
            </Col>
            <Col span={14}>
              <PermissionTable
                selectedRowKeys={permissionIds}
                onAuthorized={this.handelAuthorized}
                // eslint-disable-next-line react/jsx-boolean-value
                defaultExpandAllRows={true}
              />
            </Col>
          </Row>
        </div>
      </BasicAdminLayout>
    );
  }
}
