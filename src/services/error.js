import { requestNoTips } from '../utils/request';
import { RequestMethod } from '../common/Enum';

export async function query(code) {
  return requestNoTips(`/api/${code}`, RequestMethod.get);
}
