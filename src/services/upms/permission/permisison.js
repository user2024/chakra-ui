/* eslint-disable import/first */
import { requestDelete, requestNoTips, requestPost, requestPut } from '../../../utils/request';
import { stringify } from 'qs';
import { UrlPrefix } from '../../../common/Enum';

export const prefix = UrlPrefix.upms.permission.permissions;

export async function query(params) {
  return requestNoTips(`${prefix}?${stringify(params)}`);
}

export async function nameExists(params) {
  return requestNoTips(`${prefix}/isNameExists?${stringify(params)}`);
}

export async function create(params) {
  return requestPost(`${prefix}`, {
    body: {
      ...params,
    },
  });
}

export async function edit(params) {
  return requestPut(`${prefix}/${params.id}`, {
    body: {
      ...params,
    },
  });
}

export async function disable(id) {
  return requestPost(`${prefix}/${id}/disable`);
}

export async function enable(id) {
  return requestPost(`${prefix}/${id}/enable`);
}

export async function del(id) {
  return requestDelete(`${prefix}/${id}`);
}
