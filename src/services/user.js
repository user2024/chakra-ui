import { requestNoTips } from '../utils/request';
import { RequestMethod } from '../common/Enum';

export async function query() {
  return requestNoTips('/api/users', RequestMethod.get);
}

export async function queryCurrent() {
  return requestNoTips('/api/currentUser', RequestMethod.get);
}
