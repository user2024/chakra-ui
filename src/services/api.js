import { stringify } from 'qs';
import { requestDelete, requestGet, requestNoTips, requestPost } from '../utils/request';
import { RequestMethod } from '../common/Enum';

export async function queryProjectNotice() {
  return requestNoTips('/api/project/notice');
}

export async function queryActivities() {
  return requestNoTips('/api/activities');
}

export async function queryRule(params) {
  return requestNoTips(`/api/rule?${stringify(params)}`);
}

export async function removeRule(params) {
  return requestDelete('/api/rule', {
    method: 'POST',
    body: {
      ...params,
      method: 'delete',
    },
  });
}

export async function addRule(params) {
  return requestDelete('/api/rule', {
    method: 'POST',
    body: {
      ...params,
      method: 'post',
    },
  });
}

export async function fakeSubmitForm(params) {
  return requestPost('/api/forms', {
    body: params,
  });
}

export async function fakeChartData() {
  return requestNoTips('/api/fake_chart_data', RequestMethod.get);
}

export async function queryTags() {
  return requestGet('/api/tags');
}

export async function queryBasicProfile() {
  return requestGet('/api/profile/basic');
}

export async function queryAdvancedProfile() {
  return requestGet('/api/profile/advanced');
}

export async function queryFakeList(params) {
  return requestGet(`/api/fake_list?${stringify(params)}`);
}

export async function fakeAccountLogin(params) {
  return requestPost('/api/upms/sso/login', {
    tips: false,
    body: params,
  });
}

export async function fakeRegister(params) {
  return requestGet('/api/register', {
    method: 'POST',
    body: params,
  });
}

export async function queryNotices() {
  return requestGet('/api/notices');
}

export async function fetchIndex() {
  return requestGet('api/upms/manage/index', { tips: false });
}
