// 常用管理页面的布局
import React from 'react';
import { Card } from 'antd';
import PageHeaderLayout from './PageHeaderLayout';
import styles from './BasicAdminLayout.less';

export default ({ children, title, queryForm }) => (
  <PageHeaderLayout title={title}>
    <Card bordered="true">
      <div className={styles.tableList}>
        <div className={styles.tableListForm}>{queryForm}</div>
        {children}
      </div>
    </Card>
  </PageHeaderLayout>
);
