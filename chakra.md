
# 开发指南

## 安装国内镜像
  
```
npm install -g cnpm --registry=https://registry.npm.taobao.org
cpm install
```


## url
- 登录: http://localhost:8000/#/user/login

## 通用实现
- 给返回值添加sucess和error属性。`src/utils/request.js:152`

## 参考资料
- 代码格式规则说明：[http://eslint.cn/docs/rules/](http://eslint.cn/docs/rules/)
- [restful接口设计规范总结](https://www.jianshu.com/p/8b769356ee67)
- [我所认为的RESTful API最佳实践](https://www.scienjus.com/my-restful-api-best-practices/)
- [es6枚举](https://github.com/rauschma/enumify)
- [ProperTypes检查规则](https://reactjs.org/docs/typechecking-with-proptypes.html)

## 实用技巧
- 解决结束标签前没有空格导致格式检查报错：`editor > code style > html > 勾上 in empty tag`
- 启动api测试框架（*小技巧，terminal中`ctrl+t`开启新的标签页*）：
  ```
  cnpm install -g roadhog-api-doc
  roadhog-api-doc start
  ```
  *webstorm 2018.1.2下启动会导致卡死，建议在系统的终端中去启动*
- 如果当前文件文件的某一行或整个文件在特殊场景下要违反eslint的规则，可以在提示`ctrl+enter`中选择最后两项的其中一项  
## 推荐插件
- CamelCase

## 常用代码
- hidden的input
  ```
  
  ```
 
## 踩过的坑
- mock模块中不能引入react，否则编译会失败。

## TO DO
- 对api进行token和sign加密
