import { getUrlParams } from '../utils';

// mock tableListDataSource
const tableListDataSource = [];
for (let i = 0; i < 46; i += 1) {
  tableListDataSource.push({
    key: i,
    disabled: i % 6 === 0,
    name: `何顺 ${i}`,
    address: `好厉害的前段${i}`,
    status: [0, 1][i % 2],
  });

  if (i === 0) {
    const user = tableListDataSource[0];
    user.children = [
      {
        key: 111,
        disabled: 1,
        name: 111,
        address: '好厉害的前段',
        status: 0,
        children: [
          {
            key: 222,
            disabled: 1,
            name: 222,
            address: 222,
            status: 1,
          },
        ],
      },
    ];
  }
}

export function getUser(req, res, u) {
  const params = getUrlParams(req, u);

  let dataSource = [...tableListDataSource];

  if (params.sorter) {
    const s = params.sorter.split('_');
    dataSource = dataSource.sort((prev, next) => {
      if (s[1] === 'descend') {
        return next[s[0]] - prev[s[0]];
      }
      return prev[s[0]] - next[s[0]];
    });
  }

  if (params.status) {
    const status = params.status.split(',');
    let filterDataSource = [];
    status.forEach(s => {
      filterDataSource = filterDataSource.concat(
        [...dataSource].filter(data => parseInt(data.status, 10) === parseInt(s[0], 10))
      );
    });
    dataSource = filterDataSource;
  }

  if (params.no) {
    dataSource = dataSource.filter(data => data.no.indexOf(params.no) > -1);
  }

  let pageSize = 10;
  if (params.pageSize) {
    pageSize = params.pageSize * 1;
  }

  const result = {
    list: dataSource,
    pagination: {
      total: dataSource.length,
      pageSize,
      current: parseInt(params.currentPage, 10) || 1,
    },
  };

  if (res && res.json) {
    res.json(result);
  } else {
    return result;
  }
}

export default getUser;
