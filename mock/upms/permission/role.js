import {ajaxResult, getIdFromUrl, getUrlParams} from '../../utils';
import {defaultPagination, UrlPrefix} from '../../../src/common/Enum';

export const prefix = UrlPrefix.upms.permission.roles;

export const roleProxy = {
  [`GET ${prefix}`]: query,
  [`POST ${prefix}`]: create,
  [`PUT ${prefix}/*`]: update,
  [`DELETE ${prefix}/*`]: del,
  [`GET ${prefix}/isNameExists`]: isNameExists,
  [`GET ${prefix}/*/permissionIds`]: listPermissionIds,
};

const list = [
  {
    id: 1,
    name: '超级管理员',
  },
  {
    id: 39,
    name: '普通用户',
  },
];

function query(req, res) {
  res.json(ajaxResult({...defaultPagination, list}));
}

export function create(req, res, u, b) {
  const body = (b && b.body) || req.body;
  body.id = Math.ceil(Math.random() * 10000);
  list.unshift(body);
  res.json(ajaxResult());
}

export function update(req, res, u, b) {
  const body = (b && b.body) || req.body;
  const index = list.findIndex(m => m.id === body.id);
  list.splice(index, 1, body);
  res.json(ajaxResult());
}

export function del(req, res) {
  const id = getIdFromUrl(req.url, -1);
  const index = list.findIndex(m => m.id === id);
  list.splice(index, 1);
  res.json(ajaxResult());
}

export function isNameExists(req, res, u) {
  const urlParams = getUrlParams(req, u);
  const includes = list.find(n => n.name === urlParams.name);
  res.json(ajaxResult(includes !== undefined));
}

export function listPermissionIds(req, res) {
  const id = getIdFromUrl(req.url, -2);
  const result = []
  if (id === 1) {
    result.push(1)
  } else {
    result.push(39)
  }
  res.json(ajaxResult(result));
}
