import {ajaxResult, getIdFromUrl, getUrlParams, updateInArray} from '../../utils';
import {
  codeOf,
  PermissionLocation,
  PermissionOpenWith,
  PermissionStatus,
  PermissionType,
  UrlPrefix,
} from '../../../src/common/Enum';

export const prefix = UrlPrefix.upms.permission.permissions;

const ID_IN_URL_INDEX = -2;

export const permissionProxy = {
  [`GET ${prefix}`]: query,
  [`POST ${prefix}`]: create,
  [`POST ${prefix}/*/disable`]: disable,
  [`POST ${prefix}/*/enable`]: enable,
  [`PUT ${prefix}/*`]: update,
  [`DELETE ${prefix}/*`]: del,
  [`GET ${prefix}/isNameExists`]: isNameExists,
};

const list = [
  {
    id: 1,
    systemId: 1,
    pid: 0,
    name: '系统组织管理',
    path: 'upms/permission',
    icon: 'tool',
    type: PermissionType.menu,
    orders: 2,
    location: PermissionLocation.left,
    openWith: PermissionOpenWith.center,
    status: PermissionStatus.normal,
    authority: 'upms',
  },
  {
    id: 39,
    systemId: 1,
    pid: 1,
    name: '权限管理',
    type: PermissionType.menu,
    permissionValue: 'upms:permission:read',
    path: 'list',
    icon: null,
    status: PermissionStatus.normal,
    openWith: PermissionOpenWith.center,
    location: PermissionLocation.left,
    orders: 39,
    authority: 'upms:permission:read',
  },
];

function query(req, res) {
  res.json(ajaxResult(list))
}

function parseEnum(permission) {
  // eslint-disable-next-line no-param-reassign
  permission.type = codeOf(PermissionType, permission.type);
  // eslint-disable-next-line no-param-reassign,no-param-reassign
  permission.location = codeOf(PermissionLocation, permission.location);
  // eslint-disable-next-line no-param-reassign
  permission.status = codeOf(PermissionStatus, permission.status);
  // eslint-disable-next-line no-param-reassign
  permission.openWith = codeOf(PermissionOpenWith, permission.openWith);
  console.log(permission);
}

export function create(req, res, u, b) {
  const body = (b && b.body) || req.body;
  body.id = Math.ceil(Math.random() * 10000);
  parseEnum(body);
  list.unshift(body);
  res.json(ajaxResult());
}

export function update(req, res, u, b) {
  const body = (b && b.body) || req.body;
  parseEnum(body);
  const index = list.findIndex(m => m.id === body.id);
  list.splice(index, 1, body);
  res.json(ajaxResult());
}

export function disable(req, res) {
  const id = getIdFromUrl(req.url, ID_IN_URL_INDEX);
  updateInArray(list, m => m.id === id, {status: {...PermissionStatus.disabled}});
  res.json(ajaxResult());
}

export function enable(req, res) {
  const id = getIdFromUrl(req.url, ID_IN_URL_INDEX);
  updateInArray(list, m => m.id === id, {status: {...PermissionStatus.normal}});
  res.json(ajaxResult());
}

export function del(req, res) {
  const id = getIdFromUrl(req.url, -1);
  const index = list.findIndex(m => m.id === id);
  list.splice(index, 1);
  res.json(ajaxResult());
}

export function isNameExists(req, res, u) {
  const urlParams = getUrlParams(req, u);
  const includes = list.find(n => n.name === urlParams.name);
  res.json(ajaxResult(includes !== undefined));
}
